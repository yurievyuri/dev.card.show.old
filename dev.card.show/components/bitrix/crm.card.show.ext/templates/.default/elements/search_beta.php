<?php
// search line
$mentionSelectorId = 'mention_'.randString(6);

?>
<div class="form-row" style="">
    <?
    $filter = [];
    $filterOption = new Bitrix\Main\UI\Filter\Options('report_list');
    $filterData = $filterOption->getFilter([]);
    foreach ($filterData as $k => $v) {
        $filter[$k] = $v;
    }

    $APPLICATION->IncludeComponent('bitrix:main.ui.filter', '', [
        'FILTER_ID' => 'report_list',
        'GRID_ID' => 'report_list',
        'FILTER' => [
            ['id' => 'DATE', 'name' => 'Дата', 'type' => 'date'],
            ['id' => 'IS_SPEND', 'name' => 'Тип операции', 'type' => 'list', 'items' => ['' => 'Любой', 'P' => 'Поступление', 'M' => 'Списание'], 'params' => ['multiple' => 'Y']],
            ['id' => 'AMOUNT', 'name' => 'Сумма', 'type' => 'number'],
            ['id' => 'PAYER_INN', 'name' => 'ИНН Плательщика', 'type' => 'number'],
            ['id' => 'PAYER_NAME', 'name' => 'Плательщик'],
        ],
        'ENABLE_LIVE_SEARCH' => true,
        'ENABLE_LABEL' => true
    ]);

    ?>
</div>


<script type="text/javascript">
    BX.addCustomEvent('BX.Main.Filter:apply', BX.delegate(function (command, params) {

        //var workarea = $('#' + command); // в command будет храниться GRID_ID из фильтра

        //alert(111);

        $.post(window.location.href, function(data){
            workarea.html($(data).find('#' + command).html());
        })
    }));
</script>