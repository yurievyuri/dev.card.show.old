<?php


namespace Dev\Call;

use Bitrix\Crm\FieldMultiTable;

/**
 * Class Functions
 * @package Dev\Call
 */
class Functions extends Card
{
    /**
     * @var array
     */
    public $arRequest, $arResult;
    /**
     * @var
     */
    private $ERRORS;
    /**
     * @var string
     */
    private $currentAction;

    /**
     * Functions constructor.
     * @param array $request
     */
    public function __construct($request = [] )
    {
        if ( !$request )
        {
            $this->setERROR( __METHOD__ , 'No valid input received');
            return [];
        }
        $this->arRequest = $request;
        $this->arResult = [];
    }

    /**
     * @return int
     */
    public function getPinValue()
    {
        $key = 'PIN_VALUE';

        if ( !$this->arRequest[$key] )
        {
            //$this->setERROR( __METHOD__ , 'Missing item id');
            //return [];
        }
        return (int)$this->arRequest[$key];
    }

    /**
     * @return mixed
     */
    public function getPinType()
    {
        $key = 'PIN_TYPE';
        if ( !$this->arRequest[$key] )
        {
            //$this->setERROR( __METHOD__ , 'Missing item type');
            //return [];
        }
        return $this->arRequest[$key];
    }

    /**
     * @param $id
     * @return array|bool
     */
    public function getContactData($id )
    {
        if ( !$this->arResult['CONTACT_DATA'][$id] )
        {
            $this->setERROR( __METHOD__ , 'Failed to get contact processing data.');
            return [];
        }
        return $this->arResult['CONTACT_DATA'][ $id ] ? : false;
    }

    /**
     * @param array $array
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function checkDublicate(array $array ) : bool
    {
        $return = parent::Search( $array['VALUE'], $array['ENTITY_ID'], $array['ELEMENT_ID'] );

        return  $return ? false : true;
    }


    /**
     * @param bool $contactId
     * @param bool $actionType
     * @return array|void
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function actionExecute($contactId = false, $actionType = false )
    {
        if ( !$contactId || !$actionType )
        {
            $this->setERROR( __METHOD__ , 'Failed to get action data.');

            return [];
        }

        $array = $this->getContactData($contactId);

        unset( $array['ID'] );
        $array['ENTITY_ID'] = $this->getPinType();
        $array['ELEMENT_ID'] = $this->getPinValue();

        if ( $this->getERROR() ) return;

        if ( !class_exists('CCrmSearch'))
            \Bitrix\Main\Loader::includeModule('crm');

        $el = new \CCrmFieldMulti();

        if ( $actionType == 'ADD' && !empty( $array ) )
        {
            if( $this->checkDublicate($array) )
                $this->arResult['UPDATE'][$this->currentAction][$actionType][$this->getPinValue()] = $el->Add( $array, ['ENABLE_NOTIFICATION' => true] );

            if($el->LAST_ERROR)
                $this->setERROR(__METHOD__, $el->LAST_ERROR);
            //$this->arResult['UPDATE'][$this->currentAction][$actionType][$this->getPinValue()] = \Bitrix\Crm\FieldMultiTable::add($array);
        }
        elseif ( $actionType == 'DELETE' && $contactId )
        {
            $this->arResult['UPDATE'][$this->currentAction][$actionType][$this->getPinValue()] = $el->Delete( $contactId, ['ENABLE_NOTIFICATION' => true] );
            if($el->LAST_ERROR)
                $this->setERROR(__METHOD__, $el->LAST_ERROR);
            //$this->arResult['UPDATE'][$this->currentAction][ $actionType ][$contactId] = \Bitrix\Crm\FieldMultiTable::delete( $contactId );
        }
        else return [];

        if ( !$el->LAST_ERROR )
            \CCrmSearch::UpdateSearch(
                array(
                    'ID' => $this->getPinValue(),
                    'CHECK_PERMISSIONS' => 'N'
                ),
                $this->getPinType(),
                true
            );
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function makeOutActions()
    {
        if ( $this->getPinValue() < 0 || !$this->getPinValue() || !$this->getPinType()) return false;

        $this->getExistDataContactById();

        foreach( $this->arResult['ACTION_FUNCTIONS'] as $entity_id => $array )
        {
            foreach ( $array as $contactId => $actionType )
            {
                if ( !$actionType ) continue;

                $this->actionExecute( $contactId, $actionType );
            }
        }
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getExistDataContactById()
    {
        $result = [];

        $db = \Bitrix\Crm\FieldMultiTable::getList([

            'filter'    => ['ID' => $this->getExtistFormContactId() ]

        ]);

        while ($item = $db->Fetch())
            $result[ $item['ID'] ] = $item;

        return $this->arResult['CONTACT_DATA'] = $result;
    }

    /**
     * @return array
     */
    function getExtistFormContactId()
    {
        $this->getCurrentAction();

        if ( !isset($this->arRequest[$this->currentAction]) )
        {
            $this->setERROR( __METHOD__ , 'Unable to retrieve data for processing.');
            return [];
        }

        $result = [];

        foreach( $this->arRequest[$this->currentAction] as $entity_type => $value )
        {
            foreach ( $value as $entity_id => $array )
            {
                $result = array_merge(array_keys($array), $result);
                $this->arResult['ACTION_FUNCTIONS'][ $entity_id ] = $array ? : false;
            }
        }

       return $this->arResult['CONTACT_ARRAY'] = array_unique($result);
    }

    /**
     * @return array|string
     */
    public function getCurrentAction()
    {
        if ( !$this->arRequest['ACTION'] ) {
            $this->setERROR( __METHOD__ , 'No action specified in the dataset');
            return [];
        }
        return $this->currentAction = strtoupper($this->arRequest['ACTION']);
    }

    /**
     * @param bool $method
     * @param bool $error
     * @param bool $key
     * @return bool
     */
    public function setERROR($method = false, $error = false, $key = false )
    {
        if ( !$error ) return false;
        $this->ERRORS[ $key ] = $method . ' :: ' . $error;
        return true;
    }

    /**
     * @return mixed
     */
    public function getERROR()
    {
        return $this->ERRORS;
    }

    /**
     * @return mixed
     */
    public function lastERROR()
    {
        return end( $this->ERRORS );
    }
}